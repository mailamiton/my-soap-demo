package com.amit.soapdemo;
import javax.jws.WebService;  
import javax.jws.soap.SOAPBinding;  
import javax.jws.soap.SOAPBinding.Style;  
import javax.jws.WebMethod;  

@WebService
@SOAPBinding(style = Style.RPC)  
public interface HelloWorld {
	@WebMethod String getHelloWorldAsString(String name);  
}
